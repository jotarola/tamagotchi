#PET PROGRAM
import random
from random import randrange

class Pet (object):
	
	excitement_reduce= 3
	excitement_max = 10
	excitement_warning= 3
	food_reduce= 2
	food_max= 10
	food_warning = 3
	vocab= ['"Grrrr..."', '"Holaaa"']

	def __init__(self, name, animal_type,gender):
		gender = random.choice(["Masculino", "Femenino"])
		
		self.gender= gender
		self.name= name
		self.animal_type = animal_type
		self.food= randrange(self.food_max)
		self.excitement = randrange(self.excitement_max)
		self.vocab= self.vocab[:]

	def __clock_tick(self):
		self.excitement -= 1
		self.food -= 1
	
	def mood(self):
		if self.food > self.food_warning and self.excitement > self.excitement_warning:
			return "Feliz"
		elif self.food < self.food_warning:
			return "Hambrient@"
		else:
			return "Aburrid@"
	def __str__(self):
		return "\nSoy" + self.name + "." + "\nY me siento" + self.mood() + "."
	def teach(self, word):
		self.vocab.append(word)
		self.__clock_tick()

	def talk(self):
	
		print("Yo soy un  ", self.animal_type, "llamado", self.name, ".",
		"Yo me siento", self.mood(), "ahora.\n")
		print(self.vocab[randrange(len(self.vocab))])
	
		self.__clock_tick()

	def feed(self):
		print("***crunch*** \n mmm. Graaacias!!!")
		meal = randrange(0, self.food_max)
		self.food += meal

		if self.food < 0:
			self.food= 0
			print("Sigo con Hambreeee!!!")
		elif self.food >= self.food_max:
			self.food = self.food_max
			print("Estoy llen@!!")
		self.__clock_tick()

	def play(self):
		print("Woohooo!")
		fun = randrange(0, self.excitement_max)
		self.excitement += fun
		if self.excitement < 0:
			self.excitement = 0
			print("Estoy Aburrid@")
		elif self.excitement >= self.excitement_max:
			self.excitement = self.excitement_max
			print("Estoy muy Feliz!!!")
		self.__clock_tick
def main ():
	pet_name = input("\nCómo deseas llamar a tu mascota?\n ")
	pet_type = input("\nQué tipo de mascota es?\n")
	pet_gender = random.choice(["Masculino", "Femenino"])
		
       
	#create a new pet:
	my_pet = Pet(pet_name, pet_type,pet_gender)

	input("Holaaaa! soy " +
	my_pet.name +
	" y soy un " +
	pet_type +
	"\n y mi genero es " +
	my_pet.gender +
	"\n y soy nuev@ aqui!!" +
	
	" \n\n Presione Enter para Continuar.")

	choice = None
	
	while choice != 0:
		print(
		"""	
		*** Interactue con su mascota***	

		1 - Alimentar a su mascota
		2 - Hablar con su mascota	
		3 - Enseñar una palabra nueva a su mascota
		4 - Jugar con su mascota	
		0 - Quit
		"""
		)
		
		choice = input("Choice: ")


		if choice == "0":
			print("Good bye")
		elif choice == "1":
			my_pet.feed()
		elif choice == "2":
			my_pet.talk()
		elif choice == "3":
			new_word = input("Qué palabra le deseas enseñar a tu mascota?\n")
			my_pet.teach(new_word)
		elif choice == "4":
			my_pet.play()
		else:
			print("Lo Siento, esa opción no es válida")

main()




 